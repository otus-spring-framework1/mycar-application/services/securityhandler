package ru.otus.mycar.security.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class UserController {
    @GetMapping("/user/login")
    public String getUserLoginPage() {
        return "/user/user-authentication";
    }

    @GetMapping("/user/success")
    public String getSuccessLoginPage() {
        return "/user/user-success";
    }
}
