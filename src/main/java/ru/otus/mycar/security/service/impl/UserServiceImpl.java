package ru.otus.mycar.security.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.otus.mycar.security.service.UserService;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
}
